[x] Autocomplete, Enter submits the form, period.
[x] Add LP21 integration.
[x] PER parsing/loading in batch
[x] Only load/parse LP21 if Filter Names is enabled
[x] Send UAs with requests
