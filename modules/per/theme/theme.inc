<?php

/**
 * @file
 * Theme hooks, helper functions and preprocess functions.
 */

/**
 * Link to PER website.
 *
 * If an objective code is given, will use the specific URL for accessing the
 * page concerning that code directly. Otherwise, will simply point to a generic
 * page.
 *
 * @param string $code
 *    (optional) If available, the PER code we should add to the URL.
 *
 * @return string
 *    And HTML-formatted string.
 */
function dsb_portal_per_theme_per_site_link($code) {
  $attributes = array(
    'attributes' => array(
      'target' => '_blank',
      'title' => t("Read more about this topic on the official Plan d'études romand website (opens in a new tab)"),
    ),
  );

  if (isset($code)) {
    $code = str_replace(array(' ', '&'), array('_', ''), $code);
    return l(
      t("More information on PER site"),
      str_replace('[CODE]', $code, variable_get('dsb_portal_per_code_url', 'http://www.plandetudes.ch/web/guest/[CODE]')),
      $attributes
    );
  }
  else {
    return l(
      t("More information on PER site"),
      variable_get('dsb_portal_per_site_url', 'http://www.plandetudes.ch'),
      $attributes
    );
  }
}

/**
 * PER curriculum cycle filter link.
 *
 * Prepare a link that can be used to filter search results by a specific
 * PER curriculum cycle. This returns an HTML formatted link.
 *
 * @param string $title
 *    The link title.
 * @param string $per_cycle
 *    The curriculum cycle.
 *
 * @return string
 *    A HTML-formatted string.
 */
function dsb_portal_per_theme_per_curriculum_cycle_filter_link($title, $per_cycle) {
  return l($title, 'dsb-portal/search', array(
    'query' => array(
      'filter' => array(
        'perCycle' => array($per_cycle),
      ),
    ),
    'attributes' => array(
      'title' => t("Search for descriptions within this cycle"),
    ),
  ));
}

/**
 * PER curriculum domain filter link.
 *
 * Prepare a link that can be used to filter search results by a specific
 * PER curriculum domain. This returns an HTML formatted link.
 *
 * @param string $title
 *    The link title.
 * @param string $per_cycle
 *    The curriculum cycle.
 * @param string $per_domain
 *    The curriculum domain.
 *
 * @return string
 *    A HTML-formatted string.
 */
function dsb_portal_per_theme_per_curriculum_domain_filter_link($title, $per_cycle, $per_domain) {
  return l($title, 'dsb-portal/search', array(
    'query' => array(
      'filter' => array(
        'perCycle' => array($per_cycle),
        'perDomain' => array($per_domain),
      ),
    ),
    'attributes' => array(
      'title' => t("Search for descriptions within this domain"),
    ),
  ));
}

/**
 * PER curriculum discipline filter link.
 *
 * Prepare a link that can be used to filter search results by a specific
 * PER curriculum discipline. This returns an HTML formatted link.
 *
 * @param string $title
 *    The link title.
 * @param string $per_cycle
 *    The curriculum cycle.
 * @param string $per_domain
 *    The curriculum domain.
 * @param string $per_discipline
 *    The curriculum discipline.
 *
 * @return string
 *    A HTML-formatted string.
 */
function dsb_portal_per_theme_per_curriculum_discipline_filter_link($title, $per_cycle, $per_domain, $per_discipline) {
  return l($title, 'dsb-portal/search', array(
    'query' => array(
      'filter' => array(
        'perCycle' => array($per_cycle),
        'perDomain' => array($per_domain),
        'perDiscipline' => array($per_discipline),
      ),
    ),
    'attributes' => array(
      'title' => t("Search for descriptions within this discipline"),
    ),
  ));
}

/**
 * PER curriculum code filter link.
 *
 * Prepare a link that can be used to filter search results by a specific
 * PER curriculum code. This returns an HTML formatted link.
 *
 * @param string $curriculum_code
 *    The curriculum code.
 * @param string $label
 *    (optional) An optional label. Defaults to the code.
 *
 * @return string
 *    A HTML-formatted string.
 */
function dsb_portal_per_theme_per_curriculum_code_filter_link($curriculum_code, $label = NULL) {
  if (!isset($label)) {
    $label = $curriculum_code;
  }
  return l($label, 'dsb-portal/search', array(
    'query' => array(
      'filter' => array(
        'perObjectiveCode' => array(str_replace(' ', '', $curriculum_code)),
      ),
    ),
    'attributes' => array(
      'title' => t("Search for descriptions with this objective"),
    ),
  ));
}

/**
 * PER curriculum progression filter link.
 *
 * Prepare a link that can be used to filter search results by a specific
 * PER progression. This returns an HTML formatted link.
 *
 * WARNING: The link title MUST be properly sanitized before passing it to this
 * function! It will be printed as-is.
 *
 * @param string $title
 *    The link title.
 * @param string $per_progression
 *    The curriculum progression.
 *
 * @return string
 *    A HTML-formatted string.
 */
function dsb_portal_per_theme_per_curriculum_progression_filter_link($title, $per_progression) {
  return l($title, 'dsb-portal/search', array(
    'query' => array(
      'filter' => array(
        'perProgression' => array($per_progression),
      ),
    ),
    'html' => TRUE,
    'attributes' => array(
      'title' => t("Search for descriptions for this exact Progression d'apprentissage"),
    ),
  ));
}
