<?php

/**
 * @file
 * Template for rendering a dsb Portal PER objective entry.
 *
 * Available variables (if not specifically noted, none are sanitized):
 * - $path: The path of this objective. This contains all parents, as well as
 *   the child items of the objective, if any.
 *
 * @ingroup themeable
 */
?>
<div class="dsb-portal-per-entry">
  <div class="dsb-portal-per-entry__parents">
    <?php foreach ($path as $element): ?>
      <?php
      if ($element['type'] == 'objectifs') {
        // Stop it here. We only want all parents of the objective.
        break;
      }
      // Must we convert the parent to a link? Or keep it plain?
      switch($element['type']) {
        case 'cycles':
          if (isset($element['term'])) {
            $cycle = $element['term']->describe()->id;
            $label = dsb_portal_per_theme_per_curriculum_cycle_filter_link(
              $element['entry']['fr'],
              $cycle
            );
            break;
          } // Fallthrough in case of else.
        case 'domaines':
          if (isset($element['term']) && isset($cycle)) {
            $domain = $element['term']->describe()->id;
            $label = dsb_portal_per_theme_per_curriculum_domain_filter_link(
              $element['entry']['fr'],
              $cycle,
              $domain
            );
            break;
          } // Fallthrough in case of else.
        case 'disciplines':
          if (isset($element['term']) && isset($cycle) && isset($domain)) {
            $discipline = $element['term']->describe()->id;
            $label = dsb_portal_per_theme_per_curriculum_discipline_filter_link(
              $element['entry']['fr'],
              $cycle,
              $domain,
              $discipline
            );
            break;
          } // Fallthrough in case of else.
        default:
          $label = check_plain(trim($element['entry']['fr']));
          break;
      }
      ?>
      <span class="
        dsb-portal-per-entry__parents__element dsb-portal-per-entry__parents__element--<?php print check_plain($element['type']); ?>"><?php print $label; ?></span>
    <?php endforeach; ?>
  </div>

  <?php
  // The $element variable should now contain the objective, unless the tree
  // stopped at the discipline level.
  if (!empty($element) && $element['type'] == 'objectifs'): ?>
    <div class="dsb-portal-per-entry__info">
      <div class="dsb-portal-per-entry__info__label">
        <?php
        if (isset($element['term'])) {
          print dsb_portal_per_theme_per_curriculum_code_filter_link(
            $element['term']->getCode(),
            $element['entry']['fr']
          );
        }
        else {
          print check_plain($element['entry']['fr']);
        }
        ?>
      </div>

      <div class="dsb-portal-per-entry__info__link">
        <?php print dsb_portal_per_theme_per_site_link(
          isset($element['code']) ?
            $element['code'] :
            NULL
        ); ?>
      </div>

      <?php
      // If the objective has any children, treat them here.
      if (!empty($element['childTaxons'])): ?>
        <ul class="dsb-portal-per-entry__info__progressions">
          <?php foreach ($element['childTaxons'] as $progression): ?>
            <li>
              <?php
              // We cannot use check_plain(), because PER uses some HTML.
              $label = strip_tags($progression['entry']['fr'], '<em><span><br>');

              if (isset($progression['term'])) {
                print dsb_portal_per_theme_per_curriculum_progression_filter_link(
                  $label,
                  $progression['term']->describe()->id
                );
              }
              else {
                print $label;
              }
              ?>
            </li>
          <?php endforeach; ?>
        </ul>
      <?php endif; ?>
    </div>
  <?php endif; ?>
</div>
