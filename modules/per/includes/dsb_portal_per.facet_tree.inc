<?php

/**
 * @file
 * Logic for rendering the facets like a tree.
 */

/**
 * Render the facets as a tree.
 *
 * @see dsb_portal_per_form_dsb_portal_rest_api_search_form_alter()
 */
function dsb_portal_per_render_facet_tree(&$form) {
  $curriculum = dsb_portal_per_get_curriculum_tree();

  // Prepare a new form structure. We need to recursively treat items in the
  // tree, and as soon as we reach the leaf, we need to check if the facet is
  // actually there. If not, we discard the path, and pass on to the next.
  $facet_tree = array();
  if (
    ($root = $curriculum->getTree()) &&
    $root->hasChildren()
  ) {
    foreach ($root->getChildren() as $cycle) {
      if ($cycle->hasChildren()) {
        foreach ($cycle->getChildren() as $domain) {
          if ($domain->hasChildren()) {
            foreach ($domain->getChildren() as $discipline) {
              if ($discipline->hasChildren()) {
                foreach ($discipline->getChildren() as $objective) {
                  if (
                    ($code = trim($objective->getCode())) &&
                    !empty($code)
                  ) {
                    if (isset($form[$code])) {
                      // We have it. Put it in the facet tree.
                      $cycle_id = $cycle->describe()->id;
                      if (!isset($facet_tree[$cycle_id])) {
                        $facet_tree[$cycle_id] = array();
                      }
                      $domain_id = $domain->describe()->id;
                      if (!isset($facet_tree[$cycle_id][$domain_id])) {
                        $facet_tree[$cycle_id][$domain_id] = array();
                      }
                      $discipline_id = $discipline->describe()->id;
                      if (!isset($facet_tree[$cycle_id][$domain_id][$discipline_id])) {
                        $facet_tree[$cycle_id][$domain_id][$discipline_id] = array();
                      }
                      $objective_id = $objective->describe()->id;
                      $facet_tree[$cycle_id][$domain_id][$discipline_id][$objective_id] = $objective;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  // Now, re-iterate through this new tree, and create the form structure.
  $new_form = array();
  foreach ($form as $key => $value) {
    // Retrieve all Form API setting keys.
    if (preg_match('/^#/', $key)) {
      $new_form[$key] = $value;
    }
  }

  foreach ($facet_tree as $cycle_id => $cycle_values) {
    if (!$cycle = $curriculum->getTree()->findChildByIdentifier($cycle_id)) {
      continue;
    }

    // See _dsb_portal_rest_api_get_facet_checkboxes() for more information on
    // the facet checkbox structure.
    $new_form[$cycle_id] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($cycle->describe()->name->fr),
      '#default_value' => isset($_GET['filter']['perCycle']) && in_array($cycle_id, $_GET['filter']['perCycle']) ? $cycle_id : 0,
      '#parents' => array('filter', 'perCycle', $cycle_id),
      '#return_value' => $cycle_id,
      // Domains are shared across cycles. So filtering only makes sense if
      // the parent cycle is also selected. If the child domain is selected,
      // automatically select this cycle.
      '#states' => array(
        'checked' => array(
          ':input[data-parent-cycle="' . $cycle_id . '"]' => array('checked' => TRUE),
        ),
      ),
    );

    foreach ($cycle_values as $domain_id => $domain_values) {
      if (!$domain = $cycle->findChildByIdentifier($domain_id)) {
        continue;
      }

      $new_form["$cycle_id:$domain_id"] = array(
        '#type' => 'checkbox',
        '#title' => check_plain($domain->describe()->name->fr),
        '#prefix' => theme('indentation', array('size' => 1)),
        '#parents' => array('filter', 'perDomain', $domain_id),
        '#return_value' => $domain_id,
        '#attributes' => array(
          'data-parent-cycle' => $cycle_id,
        ),
        // Disciplines are shared across domains. So filtering only makes sense
        // if the parent domain is also selected. If the child discipline is
        // selected, automatically select this domain.
        '#states' => array(
          'checked' => array(
            ':input[data-parent-domain="' . "$cycle_id:$domain_id" . '"]' => array('checked' => TRUE),
          ),
        ),
      );

      foreach ($domain_values as $discipline_id => $discipline_values) {
        if (!$discipline = $domain->findChildByIdentifier($discipline_id)) {
          continue;
        }

        $new_form["$cycle_id:$domain_id:$discipline_id"] = array(
          '#type' => 'checkbox',
          '#title' => check_plain($discipline->describe()->name->fr),
          '#prefix' => theme('indentation', array('size' => 2)),
          '#parents' => array('filter', 'perDiscipline', $discipline_id),
          '#return_value' => $discipline_id,
          '#attributes' => array(
            'data-parent-domain' => "$cycle_id:$domain_id",
          ),
        );

        foreach ($discipline_values as $objective_id => $objective) {
          $new_form[$objective->getCode()] = $form[$objective->getCode()];
          $new_form[$objective->getCode()]['#prefix'] = theme('indentation', array('size' => 3));
        }
      }
    }
  }

  $form = $new_form;
}
