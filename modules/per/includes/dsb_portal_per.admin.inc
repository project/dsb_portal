<?php

/**
 * @file
 * Administration pages and forms.
 */

/**
 * Module settings form callback.
 */
function dsb_portal_per_admin_settings_form($form, $form_state) {
  // Make sure dependencies are loaded before configuring the module.
  if (
    !dsb_portal_check_libraries(TRUE) ||
    !dsb_portal_check_module_dependency(TRUE)
  ) {
    drupal_set_message(t("Please correct the dependency errors before configuring dsb Portal PER."), 'error');
    return array();
  }

  $form['dsb_portal_per_search_alter'] = array(
    '#type' => 'checkbox',
    '#title' => t("Activate PER objective code filter in search query"),
    '#description' => t("If a user types an objective code in the full-text search, this will activate the PER objective code filter. If this is not activated, the full-text will simply be handled by the REST API, but it may not recognize the code as such."),
    '#default_value' => variable_get('dsb_portal_per_search_alter', FALSE),
  );

  $form['facets'] = array(
    '#type' => 'fieldset',
    '#title' => t("PER facets"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['facets']['dsb_portal_per_facets'] = array(
    '#type' => 'checkboxes',
    '#title' => t("Activate facets"),
    '#description' => t("The national catalog has different facets related to the PER curriculum that can be activated for search. These will be displayed as a series of checkboxes on the search form."),
    '#options' => array(
      'per_full' => t("PER full curriculum", array(), array('context' => 'dsb_portal_per:search')),
      'perCycle' => t("PER cycle", array(), array('context' => 'dsb_portal_per:search')),
      'perObjectiveCode' => t("PER objective code", array(), array('context' => 'dsb_portal_per:search')),
      'perDomain' => t("PER domain", array(), array('context' => 'dsb_portal_per:search')),
      'perDiscipline' => t("PER discipline", array(), array('context' => 'dsb_portal_per:search')),
    ),
    '#default_value' => variable_get('dsb_portal_per_facets', array('per_full' => 'per_full')),
  );

  $form['site'] = array(
    '#type' => 'fieldset',
    '#title' => t("PER official site"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['site']['dsb_portal_per_site_url'] = array(
    '#type' => 'textfield',
    '#title' => t("Official PER site URL"),
    '#description' => t("The URL at which the PER can be visited."),
    '#default_value' => variable_get('dsb_portal_per_site_url', 'http://www.plandetudes.ch'),
  );

  $form['site']['dsb_portal_per_code_url'] = array(
    '#type' => 'textfield',
    '#title' => t("Official PER site URL for a specific code"),
    '#description' => t("The URL where to find more information about a specific PER objective code. Use the [CODE] token to specify where the objective code should be placed in the URL. For example: <em>http://www.plandetudes.ch/web/guest/[CODE]</em>."),
    '#default_value' => variable_get('dsb_portal_per_code_url', 'http://www.plandetudes.ch/web/guest/[CODE]'),
  );

  return system_settings_form($form);
}

/**
 * Page callback: trigger the batch for importing the PER curriculum.
 */
function dsb_portal_per_load_data_trigger() {
  $batch = array(
    'title' => t("Import PER data"),
    'operations' => array(
      array('dsb_portal_per_batch_load_data_step1', array()),
      array('dsb_portal_per_batch_load_data_step2', array()),
    ),
    'file' => drupal_get_path('module', 'dsb_portal_per') . '/includes/dsb_portal_per.admin.inc',
  );
  batch_set($batch);

  // Redirect to the status page when finished.
  batch_process('admin/reports/status');
}

/**
 * Batch operation for importing PER data, step 1.
 *
 * This will load the domains and disciplines.
 */
function dsb_portal_per_batch_load_data_step1(&$context) {
  if (!method_exists('Educa\DSB\Client\Curriculum\PerCurriculum', 'prepareFetch')) {
    $context['finished'] = 1;
    drupal_set_message(t("Please make sure you are using dsb Client 0.20.0 or greater."), 'error');
    return;
  }

  $context['message'] = t("Importing domains and disciplines...");

  list(
    $root,
    $dictionary
  ) = Educa\DSB\Client\Curriculum\PerCurriculum::prepareFetch();

  list(
    $root,
    $dictionary,
    $objective_ids
  ) = Educa\DSB\Client\Curriculum\PerCurriculum::fetchDomainsAndDisciplines(
    DSB_PORTAL_PER_BDPER_ENDPOINT,
    $root,
    $dictionary
  );

  // The sandbox is not kept between batch operations, and arbitrary keys are
  // discarded. Only "results" is kept, so we use it instead.
  $context['results']['root'] = $root;
  $context['results']['dictionary'] = $dictionary;
  $context['results']['objective_ids'] = $objective_ids;
}

/**
 * Batch operation for importing PER data, step 2.
 *
 * This will load the objectives and progressions.
 */
function dsb_portal_per_batch_load_data_step2(&$context) {
  if (!method_exists('Educa\DSB\Client\Curriculum\PerCurriculum', 'prepareFetch')) {
    $context['finished'] = 1;
    return;
  }

  $context['message'] = t("Importing objectives and progressions...");

  // See dsb_portal_per_batch_load_data_step1 for an explanation on why we use
  // "results" and not "sandbox" for some data.
  if (!isset($context['sandbox']['max'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($context['results']['objective_ids']);
  }

  $objective_ids = array_splice(
    $context['results']['objective_ids'],
    0,
    DSB_PORTAL_PER_BDPER_ENDPOINT_CALL_LIMIT
  );

  if (empty($objective_ids)) {
    // Nothing left to import. Finished.
    $context['finished'] = 1;
  }
  else {
    list(
      $root,
      $dictionary
    ) = Educa\DSB\Client\Curriculum\PerCurriculum::fetchObjectivesAndProgressions(
      DSB_PORTAL_PER_BDPER_ENDPOINT,
      $context['results']['root'],
      $context['results']['dictionary'],
      $objective_ids
    );

    $context['sandbox']['progress'] += count($objective_ids);
    $context['results']['root'] = $root;
    $context['results']['dictionary'] = $dictionary;

    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

  if ($context['finished'] == 1) {
    $curriculum = new Educa\DSB\Client\Curriculum\PerCurriculum($root);
    $curriculum->setCurriculumDictionary($dictionary);

    cache_set(
      DSB_PORTAL_CACHE_CURRICULUM_CID,
      $curriculum,
      'cache_curricula'
    );

    drupal_set_message(t("Successfully imported the PER curriculum data"));
  }
}
