<?php

/**
 * @file
 * PER curriculum integration for the dsb Portal.
 *
 * This module integrates the PER (Plan d'étude romand) with the dsb Portal.
 * It allows users to enable facets, showing them in a tree structure instead
 * of the default list. It also enables the rendering of the PER curriculum data
 * for a LOM-CH description.
 */

define('DSB_PORTAL_PER_BDPER_ENDPOINT', 'https://bdper.plandetudes.ch/api/v1/');
define('DSB_PORTAL_PER_BDPER_ENDPOINT_CALL_LIMIT', 20);
define('DSB_PORTAL_CACHE_CURRICULUM_CID', 'dsb_portal_per:curriculum_tree');
define('DSB_PORTAL_CACHE_CODES_CID', 'dsb_portal_per:curriculum_codes');

/**
 * Implements hook_menu().
 */
function dsb_portal_per_menu() {
  return array(
    'admin/config/services/dsb-portal-per' => array(
      'title' => 'dsb Portal PER',
      'description' => 'Manage dsb portal PER integration settings.',
      'access arguments' => array('administer dsb_portal_per'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('dsb_portal_per_admin_settings_form'),
      'file' => 'includes/dsb_portal_per.admin.inc',
    ),
    'admin/dsb-portal-per/load-per-data' => array(
      // As this page can be called directly after the installation, the user
      // may not have any of our custom permissions yet. But, if the user has
      // access to administer the site, it is safe to assume she can import a
      // few vocabularies. This is why we simply use the 'administer site
      // configuration' permission here.
      'access arguments' => array('administer site configuration'),
      'page callback' => 'dsb_portal_per_load_data_trigger',
      'file' => 'includes/dsb_portal_per.admin.inc',
      'type' => MENU_CALLBACK,
    ),
  );
}

/**
 * Implements hook_permission().
 */
function dsb_portal_per_permission() {
  return array(
    'administer dsb_portal_per' => array(
      'title' => t('Administer the dsb portal PER module settings'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function dsb_portal_per_theme() {
  $path = drupal_get_path('module', 'dsb_portal_per') . '/theme';

  $default = array(
    'path' => $path,
    'file' => 'theme.inc',
  );

  return array(
    'dsb_portal_per_entry' => array(
      'variables' => array('path' => NULL),
      'template' => 'dsb-portal-per-entry',
    ) + $default,
  );
}

/**
 * Implements hook_dsb_portal_rest_api_facets_alter().
 */
function dsb_portal_per_dsb_portal_rest_api_facets_alter(&$facets) {
  $list = array_filter(array_values(variable_get('dsb_portal_per_facets', array())));
  if (!empty($list)) {
    // per_full corresponds to perObjectiveCode and perCycle, but with an
    // enhanced rendering further down the road. See
    // dsb_portal_per_form_dsb_portal_rest_api_search_form_alter(). If it is
    // "activated" as a facet, enable perObjectiveCode and perCycle instead, and
    // not per_full.
    if (in_array('per_full', $list)) {
      unset($list[array_search('per_full', $list)]);
      $list[] = 'perObjectiveCode';
      $list[] = 'perCycle';

      // Make sure we don't add duplicates.
      $list = array_unique($list);
    }

    // Activate the facets.
    $facets = array_merge($facets, $list);
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for dsb_portal_rest_api_search_form().
 *
 * @see dsb_portal_per_render_facet_tree()
 */
function dsb_portal_per_form_dsb_portal_rest_api_search_form_alter(&$form) {
  // Add human-readable names for the facet groups.
  if (isset($form['filter_group_perDomain'])) {
    $form['filter_group_perDomain']['#title'] = t("PER domain", array(), array('context' => 'dsb_portal_per:search'));
  }

  if (isset($form['filter_group_perDiscipline'])) {
    $form['filter_group_perDiscipline']['#title'] = t("PER discipline", array(), array('context' => 'dsb_portal_per:search'));
  }

  if (isset($form['filter_group_perCycle'])) {
    // Enhance the title. This might get undone below.
    $form['filter_group_perCycle']['#title'] = t("PER cycle", array(), array('context' => 'dsb_portal_per:search'));
  }

  if (isset($form['filter_group_perObjectiveCode'])) {
    // Enhance the title. This might get undone below.
    $form['filter_group_perObjectiveCode']['#title'] = t("PER objective code", array(), array('context' => 'dsb_portal_per:search'));
  }

  // Get the facets settings. If per_full is set, we copy the PER objective
  // codes form structure and create a new tree structure.
  $list = array_filter(array_values(variable_get('dsb_portal_per_facets', array())));
  if (in_array('per_full', $list)) {
    // If the objective codes aren't present, skip.
    if (empty($form['filter_group_perObjectiveCode'])) {
      return;
    }

    module_load_include('inc', 'dsb_portal_per', 'includes/dsb_portal_per.facet_tree');

    // Copy the objective codes facet tree. We base our full tree on it.
    $form['filter_group_perFull'] = $form['filter_group_perObjectiveCode'];

    // Change the title.
    $form['filter_group_perFull']['#title'] = t("PER curriculum", array(), array('context' => 'dsb_portal_per:search'));

    // Rerender the checkboxes.
    dsb_portal_per_render_facet_tree($form['filter_group_perFull']);

    // Do we need to keep the objective codes facet tree? If not, remove it.
    if (!in_array('perObjectiveCode', $list)) {
      unset($form['filter_group_perObjectiveCode']);
    }

    // Do we need to keep the cycle facet tree? If not, remove it.
    if (!in_array('perCycle', $list)) {
      unset($form['filter_group_perCycle']);
    }
  }
}

/**
 * Implements hook_dsb_portal_active_filter_names_alter().
 */
function dsb_portal_per_dsb_portal_active_filter_names_alter(&$filter_name, $context) {
  if ($context['filter_group'] == 'label') {
    switch ($context['filter_name']) {
      case 'perDomain':
        $filter_name = t("PER domain", array(), array('context' => 'dsb_portal_per:search'));
        break;

      case 'perCycle':
        $filter_name = t("PER cycle", array(), array('context' => 'dsb_portal_per:search'));
        break;

      case 'perDiscipline':
        $filter_name = t("PER discipline", array(), array('context' => 'dsb_portal_per:search'));
        break;

      case 'perObjectiveCode':
        $filter_name = t("PER objective code", array(), array('context' => 'dsb_portal_per:search'));
        break;

      case 'perObjective':
        $filter_name = t("PER objective", array(), array('context' => 'dsb_portal_per:search'));
        break;

      case 'perProgression':
        $filter_name = t("PER learning progression", array(), array('context' => 'dsb_portal_per:search'));
        break;
    }
  }
  else {
    if (!$curriculum = dsb_portal_per_get_curriculum_tree()) {
      // Empty cache.
      watchdog(
        'dsb_portal_per',
        "No curriculum data in cache. This means we cannot alter facet names with user friendly ones. Refresh it by going to the site reports.",
        array(),
        WATCHDOG_WARNING,
        l(t("Refresh the cache"), 'admin/dsb-portal-per/load-per-data')
      );
      return;
    }
    $terms = array();
    switch ($context['filter_group']) {
      case 'perDomain':
        $terms = $curriculum->getTree()->findChildrenByTypeRecursive('domaines');
        break;

      case 'perCycle':
        $terms = $curriculum->getTree()->findChildrenByTypeRecursive('cycles');
        break;

      case 'perDiscipline':
        $terms = $curriculum->getTree()->findChildrenByTypeRecursive('disciplines');
        break;

      case 'perObjective':
        $terms = $curriculum->getTree()->findChildrenByTypeRecursive('objectifs');
        break;

      case 'perProgression':
        $terms = $curriculum->getTree()->findChildrenByTypeRecursive('progressions');
        break;
    }

    if (!empty($terms)) {
      $filter_name = array_reduce($terms, function($carry, $term) use($filter_name) {
        if ($term->describe()->id == $filter_name) {
          $carry = $term->describe()->name->fr;
        }
        return $carry;
      }, $filter_name);
    }
  }
}

/**
 * Implements hook_preprocess_dsb_portal_description_classification_info().
 */
function dsb_portal_per_preprocess_dsb_portal_description_classification_info(&$vars) {
  $lom = $vars['description'];
  $paths = array();

  $curriculum_data = $lom->getField('curriculum');
  if (!empty($curriculum_data)) {
    // Do we have the PER data cached? If not, abort.
    if (!$curriculum = dsb_portal_per_get_curriculum_tree()) {
      watchdog(
        'dsb_portal_per',
        "No curriculum data in cache. This means we cannot render PER information for descriptions. Refresh it by going to the site reports.",
        array(),
        WATCHDOG_WARNING,
        l(t("Refresh the cache"), 'admin/dsb-portal-per/load-per-data')
      );
      return;
    }

    // We recursively parse the PER tree, and store the information in "paths".
    // We split at objectives, so anything up to there can be "duplicated".
    $paths = array();

    // Does this use the new format?
    if (!$lom->isLegacyLOMCHFormat()) {
      foreach ($curriculum_data as $j => $entry) {
        if ($lom->getField("curriculum.{$j}.source.name") == 'per') {
          // Prepare a function for recursively parsing items.
          $recursive = function($item, $parents) use($curriculum, &$recursive, &$paths) {
            // Because PER doesn't have unique IDs across the entire tree, we
            // first need to load all elements with the same type, and then
            // search for the ID.
            $terms = $curriculum->getTree()->findChildrenByTypeRecursive($item['type']);
            foreach ($terms as $term) {
              if ($item['id'] == $term->describe()->id) {
                $item['term'] = $term;
                if ($item['type'] == 'objectifs') {
                  $item['code'] = $term->getCode();
                }
                break;
              }
            }

            // We arrived at a leaf. Store the path and return.
            if (empty($item['childTaxons'])) {
              $paths[] = array_merge($parents, array($item));
              return;
            }
            elseif ($item['type'] == 'objectifs') {
              // We arrived at an objective. This is where we split. Store the
              // path and any child terms, and return. Here again, find the term
              // associated with the item.
              foreach ($item['childTaxons'] as &$child) {
                $terms = $curriculum->getTree()->findChildrenByTypeRecursive($child['type']);
                foreach ($terms as $term) {
                  if ($child['id'] == $term->describe()->id) {
                    $child['term'] = $term;
                    break;
                  }
                }
              }
              $paths[] = array_merge($parents, array($item));
              return;
            }

            // Each child represents a new path. Recursively treat them.
            foreach ($item['childTaxons'] as $child) {
              // Pass an updated-on-the-fly parents array using array_merge(),
              // instead of copying and pushing.
              $recursive($child, array_merge(
                $parents,
                array($item)
              ));
            };
          };

          foreach ($entry['taxonTree'] as $item) {
            $recursive($item, array());
          }
        }
      }
    }
    else {
      foreach ($curriculum_data as $classification_entry) {
        if ($classification_entry['source'] == 'per') {
          $tree = json_decode($classification_entry['entity'], TRUE);
          foreach ($tree as $cycle_name => $domains) {
            $cycle = array(
              'id' => 'n/a',
              'type' => 'cycles',
              'purpose' => array(
                'source' => 'LOM-CHv1.2',
                'value' => 'educational level',
              ),
              'entry' => array('fr' => $cycle_name),
            );

            foreach ($domains as $domain_name => $objectives) {
              $domain = array(
                'id' => 'n/a',
                'type' => 'domaines',
                'purpose' => array(
                  'source' => 'LOM-CHv1.2',
                  'value' => 'discipline',
                ),
                'entry' => array('fr' => $domain_name),
              );

              foreach ($objectives as $code => $info) {
                $discipline = array(
                  'id' => 'n/a',
                  'type' => 'disciplines',
                  'purpose' => array(
                    'source' => 'LOM-CHv1.2',
                    'value' => 'discipline',
                  ),
                  'entry' => array('fr' => $info['discipline']),
                );

                $objective = array(
                  'id' => 'n/a',
                  'type' => 'objectifs',
                  'purpose' => array(
                    'source' => 'LOM-CHv1.2',
                    'value' => 'objective',
                  ),
                  'entry' => array(
                    'fr' => sprintf(
                      '%s (%s)',
                      $info['object'],
                      $code
                    ),
                  ),
                  'code' => $code,
                  'childTaxons' => array(),
                );

                // Fetch the progressions.
                if (!empty($info['object_elements'])) {
                  foreach ($info['object_elements'] as $values) {
                    if (!empty($values['details'])) {
                      foreach ($values['details'] as $value) {
                        $objective['childTaxons'][] = array(
                          'id' => 'n/a',
                          'type' => 'progressions',
                          'purpose' => array(
                            'source' => 'LOM-CHv1.2',
                            'value' => 'objective',
                          ),
                          'entry' => array(
                            'fr' => nl2br(preg_replace('/\n+/', "\n", $value['text'])),
                          ),
                        );
                      }
                    }
                  }
                }

                // Store the path.
                $paths[] = array($cycle, $domain, $discipline, $objective);
              }
            }
          }
        }
      }
    }
  }

  if (!empty($paths)) {
    $vars['children'][] = array(
      '#attached' => array('css' => array(
        drupal_get_path('module', 'dsb_portal_per') . '/css/dsb_portal_per.css',
      )),
      'label' => array(
        '#markup' => '<h2>' . t("Plan d'études Romand") . '</h2>',
      ),
      'paths' => array_map(function($path) {
        return array(
          '#markup' => theme('dsb_portal_per_entry', array('path' => $path)),
        );
      }, $paths),
    );
  }
}

/**
 * Implements hook_dsb_portal_rest_api_query_alter().
 *
 * Alter the search query by removing all PER codes. This works along with
 * dsb_portal_per_dsb_portal_rest_api_filters_alter(), which will in turn
 * activate the corresponding filters.
 */
function dsb_portal_per_dsb_portal_rest_api_query_alter(&$query) {
  if (variable_get('dsb_portal_per_search_alter', FALSE)) {
    $codes = dsb_portal_per_get_codes();
    $found = dsb_portal_per_extract_codes($query, array_keys($codes));
    $query = str_replace($found, '', $query);
  }
}

/**
 * Implements hook_dsb_portal_rest_api_filters_alter().
 */
function dsb_portal_per_dsb_portal_rest_api_filters_alter(&$filters) {
  if (variable_get('dsb_portal_per_search_alter', FALSE)) {
    // We get the query from the globals. The globals weren't altered by
    // dsb_portal_per_dsb_portal_api_query_alter(), so we can safely rely on
    // it, regardless of the order in which the alter hooks are called.
    list($query,,) = array_values(
      dsb_portal_rest_api_get_search_from_globals()
    );

    // Extract the codes.
    $codes = dsb_portal_per_get_codes();
    $found = dsb_portal_per_extract_codes($query, array_keys($codes));
    if (!empty($found)) {
      if (!isset($filters['perObjectiveCode'])) {
        $filters['perObjectiveCode'] = array();
      }
      foreach ($found as $code) {
        $filters['perObjectiveCode'][] = $codes[$code];
      }
    }
  }
}

/**
 * Get the official PER objective code tree.
 *
 * @return Educa\DSB\Client\Curriculum\PerCurriculum|null
 *    The PER curriculum tree, or null if it wasn't cached.
 */
function dsb_portal_per_get_curriculum_tree() {
  $data = cache_get(DSB_PORTAL_CACHE_CURRICULUM_CID, 'cache_curricula');

  if (!empty($data->data)) {
    return $data->data;
  }
  else {
    return NULL;
  }
}

/**
 * Extracts all valid PER codes from a string.
 *
 * @param string $string
 *    The string from which to extract the PER codes.
 * @param array $codes
 *    A list of PER codes to extract.
 *
 * @return array
 *    A list of found PER codes.
 */
function dsb_portal_per_extract_codes($string, $codes) {
  $found = array();
  preg_match_all('/(' . implode('|', $codes) . ')/', $string, $found);
  return $found[0];
}

/**
 * Return a list of PER codes, found in the official XML.
 *
 * This data is cached.
 *
 * @return array
 *    A list of all PER codes.
 */
function dsb_portal_per_get_codes() {
  $data = cache_get(DSB_PORTAL_CACHE_CODES_CID);

  if (!empty($data->data)) {
    return $data->data;
  }
  else {
    $curriculum = dsb_portal_per_get_curriculum_tree();

    $codes = array();
    if (
      ($root = $curriculum->getTree()) &&
      $root->hasChildren()
    ) {
      foreach ($root->getChildren() as $cycle) {
        if ($cycle->hasChildren()) {
          foreach ($cycle->getChildren() as $domain) {
            if ($domain->hasChildren()) {
              foreach ($domain->getChildren() as $discipline) {
                if ($discipline->hasChildren()) {
                  foreach ($discipline->getChildren() as $objective) {
                    if (
                      ($code = trim($objective->getCode())) &&
                      !empty($code)
                    ) {
                      $codes[$code] = $code;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    cache_set(DSB_PORTAL_CACHE_CODES_CID, $codes, 'cache_curricula');

    return $codes;
  }
}
