<?php

/**
 * @file
 *
 */

/**
 * Module settings form callback.
 */
function dsb_portal_lp21_admin_settings_form($form, $form_state) {
  // Make sure dependencies are loaded before configuring the module.
  if (
    !dsb_portal_check_libraries(TRUE) ||
    !dsb_portal_check_module_dependency(TRUE)
  ) {
    drupal_set_message(t("Please correct the dependency errors before configuring dsb Portal Lehrplan 21."), 'error');
    return array();
  }

  $form['facets'] = array(
    '#type' => 'fieldset',
    '#title' => t("Lehrplan 21 facets"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['facets']['dsb_portal_lp21_facets'] = array(
    '#type' => 'checkboxes',
    '#title' => t("Activate facets"),
    '#description' => t("The national catalog has different facets related to the Lehrplan 21 curriculum that can be activated for search. These will be displayed as a series of checkboxes on the search form."),
    '#options' => array(
      'lp21Fachbereich' => t("Lehrplan 21 Fachbereich", array(), array('context' => 'dsb_portal_lp21:search')),
      'lp21Fach' => t("Lehrplan 21 Fach", array(), array('context' => 'dsb_portal_lp21:search')),
      'lp21Kompetenzbereich' => t("Lehrplan 21 Kompetenzbereich", array(), array('context' => 'dsb_portal_lp21:search')),
      'lp21HandlungsThemenaspekt' => t("Lehrplan 21 Handlungs-und-Themenaspekt", array(), array('context' => 'dsb_portal_lp21:search')),
      'lp21Kompetenz' => t("Lehrplan 21 Kompetenz", array(), array('context' => 'dsb_portal_lp21:search')),
      'lp21Kompetenzstufe' => t("Lehrplan 21 Kompetenzstuf", array(), array('context' => 'dsb_portal_lp21:search')),
    ),
    '#default_value' => variable_get('dsb_portal_lp21_facets', array()),
  );

  $form['site'] = array(
    '#type' => 'fieldset',
    '#title' => t("Lehrplan 21 official site"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['site']['dsb_portal_lp21_site_url'] = array(
    '#type' => 'textfield',
    '#title' => t("Official Lehrplan 21 site URL"),
    '#description' => t("The URL at which the Lehrplan 21 can be visited."),
    '#default_value' => variable_get('dsb_portal_lp21_site_url', 'http://v-ef.lehrplan.ch/'),
  );

  $form['site']['dsb_portal_lp21_code_url'] = array(
    '#type' => 'textfield',
    '#title' => t("Official Lehrplan 21 site URL for a specific code"),
    '#description' => t("The URL where to find more information about a specific Lehrplan 21 element. Use the [UID] token to specify where the objective code should be placed in the URL. For example: <em>http://v-ef.lehrplan.ch/[UID]</em>."),
    '#default_value' => variable_get('dsb_portal_lp21_code_url', 'http://v-ef.lehrplan.ch/[UID]'),
  );

  return system_settings_form($form);
}
