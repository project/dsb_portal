<?php

/**
 * @file
 * Theme hooks, helper functions and preprocess functions.
 */

/**
 * LP21 curriculum filter link.
 *
 * Prepare a link that can be used to filter search results by a specific
 * LP21 element. This returns an HTML formatted link.
 *
 * @param string $title
 *    The link title.
 * @param string $type
 *    The element type.
 * @param string $id
 *    The element identifier.
 *
 * @return string
 *    A HTML-formatted string.
 */
function dsb_portal_lp21_theme_curriculum_filter_link($title, $type, $id) {
  switch ($type) {
    case 'fachbereich':
      $filter = 'lp21Fachbereich';
      break;
    case 'fach':
      $filter = 'lp21Fach';
      break;
    case 'kompetenzbereich':
      $filter = 'lp21Kompetenzbereich';
      break;
    case 'handlungs-themenaspekt':
      $filter = 'lp21HandlungsThemenaspekt';
      break;
    case 'kompetenz':
      $filter = 'lp21Kompetenz';
      break;
    case 'kompetenzstufe':
      $filter = 'lp21Kompetenzstufe';
      break;
    default:
      // We won't filter this one.
      return check_plain(trim($title));
  }

  // Fetch the human readable name. Re-use the active filter logic.
  $human_type = '';
  dsb_portal_lp21_dsb_portal_active_filter_names_alter(
    $human_type,
    array(
      'filter_group' => 'label',
      'filter_name' => $filter,
    )
  );

  return l($title, 'dsb-portal/search', array(
    'query' => array(
      'filter' => array(
        $filter => array($id),
      ),
    ),
    'attributes' => array(
      'title' => t("Search for descriptions with this specific !type", array(
        '!type' => $human_type,
      )),
    ),
  ));
}

/**
 * LP21 curriculum domain filter link.
 *
 * Prepare a link that can be used to filter search results by a specific
 * LP21 curriculum domain. This returns an HTML formatted link.
 *
 * @param string $title
 *    The link title.
 * @param string $per_cycle
 *    The curriculum cycle.
 * @param string $per_domain
 *    The curriculum domain.
 *
 * @return string
 *    A HTML-formatted string.
 */
function dsb_portal_lp21_theme_per_curriculum_domain_filter_link($title, $per_cycle, $per_domain) {
  return l($title, 'dsb-portal/search', array(
    'query' => array(
      'filter' => array(
        'perCycle' => array($per_cycle),
        'perDomain' => array($per_domain),
      ),
    ),
    'attributes' => array(
      'title' => t("Search for descriptions within this domain"),
    ),
  ));
}

/**
 * LP21 curriculum discipline filter link.
 *
 * Prepare a link that can be used to filter search results by a specific
 * LP21 curriculum discipline. This returns an HTML formatted link.
 *
 * @param string $title
 *    The link title.
 * @param string $per_cycle
 *    The curriculum cycle.
 * @param string $per_domain
 *    The curriculum domain.
 * @param string $per_discipline
 *    The curriculum discipline.
 *
 * @return string
 *    A HTML-formatted string.
 */
function dsb_portal_lp21_theme_per_curriculum_discipline_filter_link($title, $per_cycle, $per_domain, $per_discipline) {
  return l($title, 'dsb-portal/search', array(
    'query' => array(
      'filter' => array(
        'perCycle' => array($per_cycle),
        'perDomain' => array($per_domain),
        'perDiscipline' => array($per_discipline),
      ),
    ),
    'attributes' => array(
      'title' => t("Search for descriptions within this discipline"),
    ),
  ));
}

/**
 * LP21 curriculum code filter link.
 *
 * Prepare a link that can be used to filter search results by a specific
 * LP21 curriculum code. This returns an HTML formatted link.
 *
 * @param string $curriculum_code
 *    The curriculum code.
 * @param string $label
 *    (optional) An optional label. Defaults to the code.
 *
 * @return string
 *    A HTML-formatted string.
 */
function dsb_portal_lp21_theme_per_curriculum_code_filter_link($curriculum_code, $label = NULL) {
  if (!isset($label)) {
    $label = $curriculum_code;
  }
  return l($label, 'dsb-portal/search', array(
    'query' => array(
      'filter' => array(
        'perObjectiveCode' => array(str_replace(' ', '', $curriculum_code)),
      ),
    ),
    'attributes' => array(
      'title' => t("Search for descriptions with this objective"),
    ),
  ));
}

/**
 * LP21 curriculum progression filter link.
 *
 * Prepare a link that can be used to filter search results by a specific
 * LP21 progression. This returns an HTML formatted link.
 *
 * WARNING: The link title MUST be properly sanitized before passing it to this
 * function! It will be printed as-is.
 *
 * @param string $title
 *    The link title.
 * @param string $per_progression
 *    The curriculum progression.
 *
 * @return string
 *    A HTML-formatted string.
 */
function dsb_portal_lp21_theme_per_curriculum_progression_filter_link($title, $per_progression) {
  return l($title, 'dsb-portal/search', array(
    'query' => array(
      'filter' => array(
        'perProgression' => array($per_progression),
      ),
    ),
    'html' => TRUE,
    'attributes' => array(
      'title' => t("Search for descriptions for this exact Progression d'apprentissage"),
    ),
  ));
}
