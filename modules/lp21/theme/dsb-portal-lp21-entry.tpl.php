<?php

/**
 * @file
 * Template for rendering a dsb Portal LP21 Kompetenz entry.
 *
 * Available variables (if not specifically noted, none are sanitized):
 * - $path: The path of this Kompetenz. This contains all parents, as well as
 *   the child items of the Kompetenz, if any.
 *
 * @ingroup themeable
 */
?>
<div class="dsb-portal-lp21-entry">
  <div class="dsb-portal-lp21-entry__parents">
    <?php foreach ($path as $element): ?>
      <?php if ($element['type'] == 'kompetenz') {
        // Stop it here. We only want all parents of the Kompetenz.
        break;
      }
      elseif ($element['type'] == 'handlungsaspekt') {
        // This is a hack, and should be removed in the future. Currently, Zebis
        // published descriptions that do not use the correct machine readable
        // types. This results in data not being correctly indexed. This is
        // particularly difficult with Handlungs-Themenaspekten, because if the
        // API doesn't recognize an element in the description curriculum tree,
        // it won't bother with its child elements either. This results in many
        // filter links not producing any results, because the API could not
        // index them. But it seems counter-intuitive, because there ARE
        // descriptions with that data. In order to prevent us from creating
        // filter links for things we know won't work, flag this entry has
        // coming from "Zebis". When this is the case, we don't render any
        // filter links for Kompetenze and Kompetenzstufe.
        $zebis_data = TRUE;
      } ?>
      <span class="
        dsb-portal-lp21-entry__parents__element dsb-portal-lp21-entry__parents__element--<?php print check_plain($element['type']); ?>"><?php print dsb_portal_lp21_theme_curriculum_filter_link(
            $element['entry']['de'],
            $element['type'],
            $element['id']
        ); ?></span>
    <?php endforeach; ?>
  </div>

  <?php
  // The $element variable should now contain the Kompetenz, unless the tree
  // stopped at the discipline level.
  if (!empty($element) && $element['type'] == 'kompetenz'): ?>
    <div class="dsb-portal-lp21-entry__info">
      <div class="dsb-portal-lp21-entry__info__label">
        <?php if (empty($zebis_data)) {
          print dsb_portal_lp21_theme_curriculum_filter_link(
            $element['entry']['de'],
            $element['type'],
            $element['id']
          );
        }
        else {
          print check_plain(trim($element['entry']['de']));
        } ?>
      </div>

      <?php
      // If the Kompetenz has any children, treat them here.
      if (!empty($element['childTaxons'])): ?>
        <ul class="dsb-portal-lp21-entry__info__kompetenztufe">
          <?php foreach ($element['childTaxons'] as $stufe): ?>
            <li>
              <?php if (empty($zebis_data)) {
                print dsb_portal_lp21_theme_curriculum_filter_link(
                  $stufe['entry']['de'],
                  $stufe['type'],
                  $stufe['id']
                );
              }
              else {
                print check_plain(trim($stufe['entry']['de']));
              } ?>
            </li>
          <?php endforeach; ?>
        </ul>
      <?php endif; ?>
    </div>
  <?php endif; ?>
</div>
