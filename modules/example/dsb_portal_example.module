<?php

/**
 * @file
 * Example module for customizing the dsb Portal.
 *
 * This module is used to demonstrate how the dsb Portal module can be enhanced
 * and altered to the specific needs of content partners.
 */

/**
 * Implements hook_dsb_portal_rest_api_facets_alter().
 *
 * By default, the dsb Portal module only requests the learningResourceType
 * facets. Many portals actually want more facets than that. Here, we add two
 * very common ones, educaSchoolLevels and educaSchoolSubjects.
 *
 * @ingroup dsb_portal_example
 */
function dsb_portal_example_dsb_portal_rest_api_facets_alter(&$facets) {
  $facets[] = 'educaSchoolLevels';
  $facets[] = 'educaSchoolSubjects';
}

/**
 * Implements hook_form_FORM_ID_alter() for dsb_portal_rest_api_search_form().
 *
 * Even though the field labels on the search form are retrieved from the
 * Ontology Server, the official, centralized repository for all interface
 * text and strings (including translations), many content partners prefer
 * using their own terminology, especially for the facets. Here, we alter the
 * search form and change the title for the facet groups.
 *
 * Furthermore, we only want to search for compulsory education descriptions. We
 * unset all educaSchoolLevels facet checkboxes that are not part of compulsory
 * education.
 *
 * @ingroup dsb_portal_example
 */
function dsb_portal_example_form_dsb_portal_rest_api_search_form_alter(&$form) {
  $form['filter_group_learningResourceType']['#title'] = t("Resource type", array(), array('context' => 'dsb_portal:search'));
  $form['filter_group_educaSchoolLevels']['#title'] = t("Context", array(), array('context' => 'dsb_portal:search'));
  $form['filter_group_educaSchoolSubjects']['#title'] = t("Subjects", array(), array('context' => 'dsb_portal:search'));

  foreach (array(
    'pre-school',
    'post compulsory education',
    'vocational schools',
    'matura_schools',
    'vocational_baccalaureate_school',
    'gymnasium',
    'middle_schools',
    'middle_school',
    'special_needs_education',
    'indipendent_of_levels ',
    'tertiary_level',
    'teacher_training_colleges',
  ) as $key) {
    if (isset($form['filter_group_educaSchoolLevels'][$key])) {
      unset($form['filter_group_educaSchoolLevels'][$key]);
    }
  }
}

/**
 * Implements hook_dsb_portal_rest_api_filters_alter().
 *
 * If no filter is active for ownerUsername (content partner filter), we add
 * one, and only show descriptions for the following content partners:
 * - andregortz@me.com
 * - archibald@bi.zh.ch
 *
 * This list is completely arbitrary, but demonstrates how this could be
 * implemented.
 *
 * Furthermore, the module only displays results that have something to do with
 * compulsory education.
 */
function dsb_portal_example_dsb_portal_rest_api_filters_alter(&$filters) {
  if (empty($filters['ownerUsername'])) {
    $filters['ownerUsername'] = array(
      'andregortz@me.com',
      'archibald@bi.zh.ch',
    );
  }

  if (!isset($filters['educaSchoolLevels'])) {
    $filters['educaSchoolLevels'] = array();
  }
  if (!in_array('compulsory education', $filters['educaSchoolLevels'])) {
    $filters['educaSchoolLevels'][] = 'compulsory education';
  }
}
