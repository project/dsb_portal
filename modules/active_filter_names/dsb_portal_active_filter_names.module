<?php

/**
 * @file
 * Enhance the active filter names on the dsb Portal search form.
 *
 * This module is used to change the machine names of the active filter recap
 * on the dsb Portal search form. This is done by querying the REST API and
 * fetch human readable data, which is cached for better performance.
 */

/**
 * Implements hook_dsb_portal_facet_name_alter().
 */
function dsb_portal_active_filter_names_dsb_portal_facet_name_alter(&$facet_name, $facet) {
  $facet_name = dsb_portal_active_filter_names_fetch_human_readable_value('label', $facet_name);
}

/**
 * Implements hook_form_FORM_ID_alter() for dsb_portal_rest_api_search_form().
 *
 * Fetch the list of active filters and process them. Transform them so we show
 * the human-readable name instead of the machine name.
 */
function dsb_portal_active_filter_names_form_dsb_portal_rest_api_search_form_alter(&$form) {
  if (!empty($form['active_filters'])) {
    foreach ($form['active_filters'] as $key1 => &$element) {
      // Don't treat items prefixed with #, as they are settings for the
      // fieldset (e.g. #title).
      if (!preg_match('/^#/', $key1)) {
        foreach ($element as $key2 => &$filter) {
          // Here again, don't treat items prefixed with #, as they are settings
          // for the filter group (e.g. #title). Also ignore the "label"
          // element, as well as 'hidden_*' elements.
          if (!preg_match('/^#/', $key2) && $key2 !== 'label' && !preg_match('/^hidden_/', $key2)) {
            // Extract the facet group first.
            $filter_group = str_replace('remove_filter_', '', preg_replace('/_\d+$/', '', $filter['#name']));

            // And extract the actual filter value. This is hard coded in HTML,
            // in the field #prefix.
            $match = array();
            preg_match('/<span.+?>(.+?)<\/span>/', $filter['#prefix'], $match);
            $filter_value = !empty($match[1]) ? $match[1] : 0;

            // Try replacing it.
            if (!empty($filter_value) && !empty($filter_group)) {
              $human_readable_value = dsb_portal_active_filter_names_fetch_human_readable_value($filter_group, $filter_value);

              $filter['#prefix'] = str_replace($filter_value, $human_readable_value, $filter['#prefix']);
            }
          }
          elseif ($key2 == 'label') {
            // There are some labels we want to update as well. This done via
            // hard-coded human readable values, so tread lightly.
            $filter['#markup'] = dsb_portal_active_filter_names_fetch_human_readable_value('label', $filter['#markup']);
          }
        }
      }
    }
  }
}

/**
 * Search for the human readable filter name.
 *
 * @param string $filter_group
 *    The group the filter belongs to. Check the official REST API for more
 *    information on available options.
 * @param string $filter_value
 *    The machine-readable value of the filter.
 *
 * @return string
 *    The human-readable value, or the machine-readable value if none could be
 *    found.
 */
function dsb_portal_active_filter_names_fetch_human_readable_value($filter_group, $filter_value) {
  global $language;
  static $facet_data = array();

  // First check the cache. What follows is pretty expensive, so if we have
  // cached data, use it.
  $cid = "dsb_portal_active_filter_names:$filter_group:$filter_value:{$language->language}";
  $data = cache_get($cid);

  if (!empty($data->data)) {
    return $data->data;
  }
  else {
    module_load_include('inc', 'dsb_portal', 'theme/theme');

    // No cache. We're going to have to look for the filter human-readable name.
    // Is the group "label"? If so, we're not actually dealing with a facet
    // or filter, but rather the label that groups them together. This is hard
    // coded, as this information is either already a human-readable string,
    // coming from the Ontology Server or a string that cannot be
    // fetched.
    if ($filter_group == 'label') {
      switch ($filter_value) {
        case 'context':
          $value = t("Education context", array(), array('context' => 'dsb_portal:view'));
          break;

        case 'difficulty':
          $value = t("Difficulty", array(), array('context' => 'dsb_portal:view'));
          break;

        case 'contributor':
          $value = t("Contributor", array(), array('context' => 'dsb_portal:search'));
          break;

        case 'coverage':
          $value = t("Coverage", array(), array('context' => 'dsb_portal:search'));
          break;

        case 'keywords':
          $value = t("Keywords", array(), array('context' => 'dsb_portal:view'));
          break;

        case 'ownerUsername':
        case 'ownerDisplayName':
          $value = t("Partner", array(), array('context' => 'dsb_portal:search'));
          break;

        default:
          $ontology_list = dsb_portal_active_filter_names_get_ontology_data();
          if (isset($ontology_list[$filter_value])) {
            $value = _dsb_portal_theme_get_langstring_value($ontology_list[$filter_value]);
          }
          break;
      }
    }
    else {
      $ontology_list = dsb_portal_active_filter_names_get_ontology_data();

      if (isset($ontology_list[$filter_value])) {
        $value = _dsb_portal_theme_get_langstring_value($ontology_list[$filter_value]);
      }
      else {
        // The filter is not part of the Ontology data. This is more complicated,
        // as we might have to extract data from fully loaded LOM descriptions.
        // Lets check.
        switch ($filter_group) {
          case 'language':
            // The base module already has a method for "prettyfying" language
            // names.
            module_load_include('inc', 'dsb_portal', 'dsb_portal.dsb_portal');
            $value = dsb_portal_dsb_portal_facet_name_alter($filter_value, NULL);
            break;

          case 'ownerDisplayName':
            // This is actually already human-readable. Simply ignore it.
            $value = $filter_value;
            break;

          case 'ownerUsername':
            // We can fetch all content partner data, and extract the name from
            // there.
            $content_partners = dsb_portal_active_filter_names_get_content_partners();
            if (isset($content_partners[$filter_value])) {
              if (!empty($content_partners[$filter_value]['company'])) {
                $value = $content_partners[$filter_value]['company'];
              }
            }
            break;
        }
      }
    }

    // Did we find a value?
    if (!empty($value)) {
      // Allow other modules to alter this data.
      $context = array(
        'filter_group' => $filter_group,
        'filter_name' => $filter_value,
      );
      drupal_alter('dsb_portal_active_filter_names', $value, $context);

      cache_set($cid, $value, 'cache', CACHE_TEMPORARY);
      return $value;
    }
  }

  // Allow other modules to alter this data.
  $context = array(
    'filter_group' => $filter_group,
    'filter_name' => $filter_value,
  );
  drupal_alter('dsb_portal_active_filter_names', $filter_value, $context);

  return $filter_value;
}

/**
 * Fetch the data from the Ontology server.
 *
 * This data is cached, and wiped at the next cache wipe.
 *
 * @return array
 *    A list of Ontology human readable names, keyed by machine readable names.
 */
function dsb_portal_active_filter_names_get_ontology_data() {
  static $list;

  // Use a static cache as well as a permanent cache. We don't want to hit the
  // cache storage multiple times.
  if (!empty($list)) {
    return $list;
  }

  $cid = 'dsb_portal_active_filter_names:ontology';
  $data = cache_get($cid);

  if (!empty($data->data)) {
    return $data->data;
  }
  else {
    $client = dsb_portal_get_client_based_on_config();
    $ontology_data = $client->loadOntologyData('list', array('lom-v1', 'educa_standard_curriculum', 'classification_system'));

    $list = array();

    // We wish to ignore the technical formats. They just create conflicts.
    unset($ontology_data['technical_format']);

    // We ignore the groups. We only want the actual items.
    foreach ($ontology_data as $values) {
      foreach ($values as $key => $value) {
        $list[$key] = $value;
      }
    }

    cache_set($cid, $list, 'cache', CACHE_TEMPORARY);
    return $list;
  }
}

/**
 * Fetch the list of content partners.
 *
 * This data is cached, and wiped at the next cache wipe.
 *
 * @return array
 *    A list of content partners, along with contact information.
 */
function dsb_portal_active_filter_names_get_content_partners() {
  static $list;

  // Use a static cache as well as a permanent cache. We don't want to hit the
  // cache storage multiple times.
  if (!empty($list)) {
    return $list;
  }

  $cid = 'dsb_portal_active_filter_names:content_partners';
  $data = cache_get($cid);

  if (!empty($data->data)) {
    return $data->data;
  }
  else {
    $client = dsb_portal_get_client_based_on_config();
    $list = $client->loadPartners();

    cache_set($cid, $list, 'cache', CACHE_TEMPORARY);
    return $list;
  }
}
