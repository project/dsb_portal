<?php

/**
 * @file
 * Contains DsbPortalTestClient.
 */

use Educa\DSB\Client\ApiClient\TestClient;

class DsbPortalTestClient extends TestClient {

  /**
   * Re-authenticate, and refresh the token.
   */
  public function reAuthenticate() {
    $this->authenticate();
  }

  /**
   * Set the identification token key.
   *
   * @param string $tokenKey
   */
  public function setTokenKey($tokenKey) {
    $this->tokenKey = $tokenKey;
    return $this;
  }

  /**
   * Get the identification token key.
   *
   * @return string
   */
  public function getTokenKey() {
    return $this->tokenKey;
  }

  /**
   * Set the identification token expiry date.
   *
   * @param string $expire
   */
  public function setTokenExpiresOn($expire) {
    $this->tokenExpiresOn = $tokenExpiresOn;
    return $this;
  }

  /**
   * Get the identification token expiry date.
   *
   * @return string
   */
  public function getTokenExpiresOn() {
    return isset($this->tokenExpiresOn) ? $this->tokenExpiresOn : '';
  }
}
